# ACID
> Back then 1 server, this rule works fine, but with large distributed system, these things need relax for sake of performance and scale.
- Atomic: All or Nothing 
- Consistency:  almost everything is eventual consistency 
UPDATE 1 database, have to wait for other replicas to change -> data in different regions have different values
- Isolation: a transaction read another transaction while updating  
Problems: 
	- Dirty read: read when the other not commited update
	- Non-Repeatable: 1st read b4 update, 2nd read after commited update 
	- Phantom Read:  same above, but insert a new row instead of updating
Solution: setting levels, performance decrease
![[Pasted image 20240226160418.png]]
### row level security 
Only allow authenticated user to modify that row 
- for e.g, if logged in user_id is in author_id col of that table, allow
### Exclusive lock and shared lock  
https://www.linkedin.com/advice/3/what-some-common-locking-scenarios-patterns-different 
Exclusive: only 1 allow read write at a time  
Shared: All allow read, no write

# Indexing BTree
https://www.youtube.com/watch?v=NI9wYuVIYcA 
Basically, non-ordered index hurt performance (UUID), but still widely used  
- auto increment is fastest, and simplest but cant work in distributed, 
- Snowflake is better uuid in terms of performance, but has high collision rate compared to uuid/uild
- UILD has time component so in order and better index/performance, lower collision than snowflake. But overall more complex that uuid

# Replicas and Sharding




# database
--- 
# Refererences 




2024 02 26 16:02
#literature [[computer science]] [[sql]] [[nosql]] [[low level]] 